import React from 'react'
import './Footer.css'

const Footer = props => 
        <footer>
            <p>Desenvolvido por <a href="https://www.linkedin.com/in/jonathas-costa-a9844ab1/" target="_blank" rel="noopener noreferrer">Jonathas Costa </a> </p>    
        </footer>

export default(Footer)
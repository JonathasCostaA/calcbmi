import React, { Component, Fragment } from 'react'
import { render } from 'react-dom'
import './CalculatorBMI.css'
import UserDetails from '../utils/userDetails'

const userDetails = UserDetails

export default class Caculator extends Component {

    state = {
        valueWeight: 0,
        valueHeight: 0,
        imc: 0,
        resultIMC: 'Preencha os campos'
    }

    constructor(props) {
        super(props)
        this.setWeight = this.setWeight.bind(this)
        this.setHeight = this.setHeight.bind(this)
        this.calcBMI = this.calcBMI.bind(this)
    }

    setWeight(event) {
        this.setState({ valueWeight: parseFloat(event.target.value) })
    }
    setHeight(event) {
        this.setState({ valueHeight: parseFloat(event.target.value) })
    }
    calcBMI(event) {
        event.preventDefault()
        if (this.state.valueHeight === 0 || this.state.valueWeight === 0) return
        let result = this.state.valueWeight / (this.state.valueHeight * this.state.valueHeight)
        this.setState({ imc: result, resultIMC: userDetails(result) })
    }

    render() {
        return (
            <Fragment>
                <form action='#' className="formBMI" onSubmit={this.calcBMI}>
                    <div className="formGroup">
                        <label htmlFor="weight">Digite seu peso</label>
                        <input type="text" name="weight" id="weight" value={this.state.value} onChange={this.setWeight} />
                    </div>

                    <div className="formGroup">
                        <label htmlFor="heigth">Digite sua altura</label>
                        <input type="text" name="height" id="height" value={this.state.value} onChange={this.setHeight} />
                    </div>
                    <button className="btnCalculator" >Calcular</button>
                    <div className="result"><p>{this.state.imc === 0 ? 'Preencha os campos!' : `O seu IMC é ${this.state.imc.toFixed(2)}, ${this.state.resultIMC}`}</p></div>
                </form>
            </Fragment>

        )

    }
}




import React from 'react'

const userDetails = imc => {
    let imcResult;
    if (imc <= 18.5) {
        imcResult = ' peso baixo! procure um nutricionista'
    }else if(imc <= 24.99){
        imcResult = ' parabéns! : )  Você esta com o peso ideal!'
    }else if(imc <= 29.99){
        imcResult = ' hunn! : | você esta acima do peso, inicie uma atividade fisica e procure um nutricionista.'
    }else if(imc <= 34.99){
        imcResult = ' obesidade nível 1 : ( , cuide-se e procure um nutricionista!'
    }else if(imc <= 39.9){
        imcResult = ' obesidade nível 2 : (, cuide-se e procure um nutricionista!'
    }else if(imc >=40){
        imcResult = ' obesidade Morbida : O, procure agora um nutricionista  ou medico clinico, lembre-se que sua vida é muito importante e que todos os dias são um recomeço!'        
    }

    return imcResult
}

export default (userDetails)
